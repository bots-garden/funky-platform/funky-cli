# Funky.CLI

## Build the CLI

```shell
sudo npm install -g pkg
pkg -t node10-linux,node10-osx,node10-win funky.cli.js 
pkg -t node10-linux,node10-osx funky.cli.js 
```

## Uee the CLI

### Deploy a function

```shell
./funky.cli-macos --path=./ola-function \
  --function=ola \
  --branch=master \
  --token=SPACESQUID \
  --platform=http://funky.eu.ngrok.io \
  --cmd=deploy
```

### Remove a function

```shell
./funky.cli-macos \
  --function=ola \
  --branch=master \
  --token=SPACESQUID \
  --platform=http://funky.eu.ngrok.io \
  --cmd=remove
```


```shell
curl --header "ADMIN_TOKEN: <TOKEN>" \
  -F "function=<FUNCTION_NAME>" \
  -F "branch=<BRANCH_NAME>" \
  -F "file=@<PATH_TO_THE_FILE_JAR>" \
  -F "file=@<PATH_TO_CONFIG.YML>" \
  <FUNKY_PLATEFORM_URL>/publish
```

### Remove a function

```shell
curl --header "ADMIN_TOKEN: <TOKEN>" \
  -X "DELETE" \
  <FUNKY_PLATEFORM_URL>/function/<FUNCTION_NAME>/<BRANCH_NAME>
```
